from django.db import models

class Berita(models.Model):
    judul = models.CharField(max_length=300)
    deskripsi = models.TextField()
    link_gambar = models.URLField(blank=True)

    def __str__(self):
        return self.judul

    class Meta:
        verbose_name = 'Berita'
        verbose_name_plural = 'Berita'

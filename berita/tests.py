from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.conf import settings
from django.core.exceptions import FieldError
from django.contrib.auth import get_user_model

from unittest import mock

from donasi.models import ProgramDonasi
from berita.models import Berita
from berita.views import daftar_berita, daftar_program_donasi_dari_berita

class BeritaModelsTestCase(TestCase):
    def test_program_donasi_saved_correctly(self):
        data = {
            'judul': 'Test Berita',
            'deskripsi': 'Deskripsi Berita',
            'link_gambar': 'https://www.sayastres.com',
        }
        berita = Berita(**data)
        berita.save()

        query = Berita.objects.filter(**data)
        self.assertEqual(1, len(query))
        self.assertEqual(berita, query[0])
        self.assertEqual(berita.judul, str(query[0]))

class BeritaViewsTestCase(TestCase):
    def setUp(self):
        user = get_user_model().objects.create_user(email='testUser@email.com', password='passwordUser', name='Test User', date_of_birth='1999-12-04')
        
        data = {
            'judul': 'Test Berita',
            'deskripsi': 'Deskripsi Berita',
            'link_gambar': 'https://www.sayastres.com',
        }
        berita = Berita(**data)
        berita.save()
        
        pd_data = {
            'inisiator': user,
            'berita_terkait': berita,
            'judul': 'Test Program Donasi',
            'deskripsi': 'Deskripsi Program Donasi',
            'link_gambar': 'https://www.sayastres.com',
            'target_donasi': 5000000
        }
        program_donasi = ProgramDonasi(**pd_data)
        program_donasi.save()

    def test_daftar_berita_url_exists(self):
        response = Client().get(reverse('berita:daftar-berita'))
        self.assertEqual(response.status_code, 200)

    def test_daftar_berita_url_use_daftar_berita_view(self):
        daftar_berita_url = resolve(reverse('berita:daftar-berita'))
        self.assertEqual(daftar_berita_url.func, daftar_berita)

    def test_daftar_berita_view_render_correctly(self):
        berita = Berita.objects.get(judul='Test Berita')

        response = Client().get(reverse('berita:daftar-berita'))
        self.assertTemplateUsed(response, 'berita/daftar_berita.html')
        self.assertContains(response, berita.judul)
        self.assertContains(response, berita.deskripsi)
        self.assertContains(response, berita.link_gambar)

    def test_daftar_program_donasi_dari_berita_url_exists(self):
        response = Client().get(reverse('berita:daftar-program-donasi-dari-berita', args=[1]))
        self.assertEqual(response.status_code, 200)

    def test_daftar_program_donasi_dari_berita_url_use_correct_view(self):
        daftar_program_donasi_dari_berita_url = resolve(reverse('berita:daftar-program-donasi-dari-berita', args=[1]))
        self.assertEqual(daftar_program_donasi_dari_berita_url.func, daftar_program_donasi_dari_berita)

    def test_daftar_program_donasi_dari_berita_view_render_correctly(self):
        berita = Berita.objects.get(judul='Test Berita')
        daftar_program_donasi = ProgramDonasi.objects.filter(berita_terkait=berita)

        response = Client().get(reverse('berita:daftar-program-donasi-dari-berita', args=[1]))
        self.assertTemplateUsed(response, 'berita/daftar_program_donasi_dari_berita.html')
        self.assertContains(response, berita.judul)
        self.assertContains(response, berita.deskripsi)
        self.assertContains(response, berita.link_gambar)
        for pd in daftar_program_donasi:
            self.assertContains(response, pd.judul)
            self.assertContains(response, pd.deskripsi)
            self.assertContains(response, pd.link_gambar)
            self.assertContains(response, format(pd.target_donasi, ","))
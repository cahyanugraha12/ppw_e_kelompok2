from django.shortcuts import render, get_object_or_404
from django.db.models import Count

from berita.models import Berita
from donasi.models import ProgramDonasi

def daftar_berita(request):
    berita_list = Berita.objects.annotate(jumlah_program_donasi=Count('daftar_program_donasi'))

    response = {
        'berita_list': berita_list
    }

    return render(request, 'berita/daftar_berita.html', response)

def daftar_program_donasi_dari_berita(request, pk):
    berita = get_object_or_404(Berita, pk=pk)
    daftar_program_donasi = ProgramDonasi.objects.filter(berita_terkait=berita)
    for pd in daftar_program_donasi:
        pd.target_donasi = format(pd.target_donasi, ",")

    response = {
        'berita': berita,
        'daftar_program_donasi' : daftar_program_donasi
    }

    return render(request, 'berita/daftar_program_donasi_dari_berita.html', response)

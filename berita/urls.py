from django.urls import re_path
from berita.views import daftar_berita, daftar_program_donasi_dari_berita

app_name = 'berita'

urlpatterns = [
    re_path(r'^berita/$', daftar_berita, name='daftar-berita'),
    re_path(r'^berita/(?P<pk>[0-9]+)/daftar-program-donasi/$', daftar_program_donasi_dari_berita, name='daftar-program-donasi-dari-berita'),
]
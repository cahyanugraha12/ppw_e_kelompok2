from django.contrib.auth.models import User

def is_logged_in(user):
    return isinstance(user, User)

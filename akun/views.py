from django.shortcuts import render, redirect

from akun.models import CustomUser
from akun.utils import is_logged_in
from akun.forms import RegistrasiForm

def homepage(request):
    response = {
        'is_logged_in': is_logged_in(request.user)
    }
    return render(request, 'homepage.html', response)

def registrasi(request):
    registrasi_form = RegistrasiForm()

    if request.method == 'POST':
        data = request.POST.copy()
        registrasi_form = RegistrasiForm(data=data)
        if registrasi_form.is_valid():
            user, created = CustomUser.objects.get_or_create(
                name=registrasi_form.cleaned_data['name'],
                date_of_birth=registrasi_form.cleaned_data['date_of_birth'],
                email=registrasi_form.cleaned_data['email'],
                password=registrasi_form.cleaned_data['password'],
            )
            return render(request, 'akun/registrasi_sukses.html')

    response = {
        'registrasi_form': registrasi_form
    }
    return render(request, 'akun/registrasi.html', response)

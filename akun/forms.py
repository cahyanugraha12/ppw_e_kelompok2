from django import forms

from akun.models import CustomUser

class RegistrasiForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ['name', 'date_of_birth', 'email', 'password']
        widgets = {
            'name': forms.TextInput(attrs={'type': 'text', 'class': 'form-control'}),
            'date_of_birth': forms.DateInput(attrs={'type': 'date', 'class': 'form-control'}), 
            'email': forms.EmailInput(attrs={'type': 'email', 'class': 'form-control'}), 
            'password': forms.PasswordInput(attrs={'type': 'password', 'class': 'form-control'}),
        }

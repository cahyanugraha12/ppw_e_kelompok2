from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.conf import settings

from unittest import mock

from akun.models import CustomUser
from akun.forms import RegistrasiForm
from akun.views import homepage, registrasi

class HomepageTestCase(TestCase):
    def test_homepage_url_exists(self):
        response = Client().get(reverse('akun:homepage'))
        self.assertEqual(response.status_code, 200)

    def test_homepage_url_use_homepage_view(self):
        homepage_url = resolve(reverse('akun:homepage'))
        self.assertEqual(homepage_url.func, homepage)

    def test_homepage_view_use_correct_template(self):
        response = Client().get(reverse('akun:homepage'))

        self.assertTemplateUsed(response, 'homepage.html')

class AkunModelsTestCase(TestCase):
    def test_custom_user_saved_correctly(self):
        user = CustomUser.objects.create_user(email='testUser@email.com', password='passwordUser', name='Test User', date_of_birth='1999-12-04')

        query = CustomUser.objects.filter(email='testUser@email.com', name='Test User')
        self.assertEqual(1, len(query))
        self.assertEqual(user, query[0])
        self.assertEqual(user.name, str(query[0]))
        self.assertEqual(user.name, query[0].get_full_name())
        self.assertEqual(user.name, query[0].get_short_name())

    def test_custom_user_create_superuser(self):
        user = CustomUser.objects.create_superuser(email='testUser@email.com', password='passwordUser', name='Test User', date_of_birth='1999-12-04')
        
        query = CustomUser.objects.filter(email='testUser@email.com', name='Test User')
        self.assertEqual(1, len(query))
        self.assertEqual(user, query[0])
        self.assertEqual(user.name, str(query[0]))
        self.assertEqual(user.name, query[0].get_full_name())
        self.assertEqual(user.name, query[0].get_short_name())

class AkunFormsTestCase(TestCase):
    def test_registrasi_form_valid(self):
        data = {
            'name': 'Test User', 
            'date_of_birth': '1999-12-04',
            'email': 'testUser@email.com',
            'password': 'passwordUser',
        }
        registrasi_form = RegistrasiForm(data=data)

        self.assertEqual(registrasi_form.is_valid(), True)
    
    def test_registrasi_form_invalid(self):
        data = {
            'name': '', 
            'date_of_birth': '',
            'email': 'testUser_error',
            'password': '',
        }
        registrasi_form = RegistrasiForm(data=data)

        self.assertEqual(registrasi_form.is_valid(), False)

class AkunViewsTestCase(TestCase):
    def test_registrasi_url_exists(self):
        response = Client().get(reverse('akun:registrasi'))
        self.assertEqual(response.status_code, 200)

    def test_registrasi_url_use_registrasi_view(self):
        registrasi_url = resolve(reverse('akun:registrasi'))
        self.assertEqual(registrasi_url.func, registrasi)

    def test_registrasi_view_render_correctly(self):
        registrasi_form = RegistrasiForm()

        response = Client().get(reverse('akun:registrasi'))
        self.assertTemplateUsed(response, 'akun/registrasi.html')
        self.assertContains(response, registrasi_form['name'])
        self.assertContains(response, registrasi_form['email'])
        self.assertContains(response, registrasi_form['password'])
        
    def test_registrasi_view_post_success(self):
        data = {
            'name': 'Test User',
            'date_of_birth': '1999-12-04',
            'email': 'testEmail@email.com',
            'password': 'hahahihi123'
        }

        response = Client().post(reverse('akun:registrasi'), data=data)

        akun_count = CustomUser.objects.all().count()
        self.assertEqual(akun_count, 1)

    def test_registrasi_view_post_failed(self):
        data = {
            'name': '',
            'date_of_birth': '',
            'email': '',
            'password': ''
        }

        response = Client().post(reverse('akun:registrasi'), data=data)

        akun_count = CustomUser.objects.all().count()
        self.assertEqual(akun_count, 0)
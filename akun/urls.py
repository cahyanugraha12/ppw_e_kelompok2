from django.urls import re_path
from akun.views import homepage, registrasi

app_name = 'akun'

urlpatterns = [
    re_path(r'^$', homepage, name='homepage'),
    re_path(r'^registrasi/$', registrasi, name='registrasi'),
]